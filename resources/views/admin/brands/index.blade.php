@extends('admin.layouts.master')
@section('content')

<div class="wrapper">
	<div class="container-fluid">
		<div class="row" style="padding-top: 50px">
			<div class="col-md-12">
				<div class="card-box">
					<div style="display: flex; vertical-align: middle; justify-content: space-between; align-items: center;">
					
						<h4  class="header-title">{{ trans('admin.brands') }}</h4>
						<a href="/{{ app()->getLocale() }}/admin/brands/create" class="btn btn-success waves-effect width-md waves-light" >{{ trans('admin.add_brand') }}</a>
					</div>
					<br>
					<br>
	
					<div class="table-responsive">
						<table class="table table-dark mb-0">
							<thead>
							<tr>
								<th>#</th>
								<th>{{ trans('admin.brand_name') }}</th>
								<th>{{ trans('admin.website_url') }}</th>
								<th>{{ trans('admin.products') }}</th>
								<th>{{ trans('admin.edit') }}</th>
								<th>{{ trans('admin.delete') }}</th>
							</tr>
							</thead>
							<tbody>
							
							@foreach ($brands as $key => $brand)
								
							<tr>
								<th>{{ $key + 1 }}</th>
								<th>{{ $brand->name }}</th>
								<th>@if($brand->website !== null)<a href="{{ $brand->website }}">{{ $brand->website }}</a> @else {{ trans('admin.no_website') }} @endif </th>
								<th><a href="/{{ app()->getLocale() }}/admin/products?brand={{ $brand->id }}" class="btn btn-success waves-effect width-md waves-light">{{ trans('admin.products') }}</a></th>
								
								<th><a href="/{{ app()->getLocale() }}/admin/brands/{{ $brand->id }}/edit" class="btn btn-success waves-effect width-md waves-light">{{ trans('admin.edit') }}</a></th>
								<th>
									<form method="post" action="/{{ app()->getLocale() }}/admin/brands/{{ $brand->id }}"> 
										@csrf
										@method('DELETE')
										<button type="submit" class="btn btn-success waves-effect width-md waves-light">{{ trans('admin.delete') }}</button>
									</form>
								</th>
							</tr>
							@endforeach
							</tbody>
						</table>
					</div>
					<br>
					{{ $brands->links() }}
				</div>
			</div>
		</div>
	</div>
</div>

@endsection 