@extends('admin.layouts.master')
@section('content')

<div class="wrapper">
	<div class="container-fluid">
		<div class="row" style="padding-top: 50px">
			<div class="col-md-12">
				<div class="card-box">
					

					<form action="/{{ app()->getLocale() }}/admin/brands/{{ $brand->id }}" method="POST">
						@csrf
						@method('PUT')
						<div class="col-xl-12">
							<h5 class="mb-3">{{ trans('admin.edit_brand') }}</h5>
							<div class="form-group">
								<label>{{ trans('admin.name') }}</label>
								<input type="text" @if($errors->has('name')) style="border: 1px solid red" @endif required class="form-control" value="{{ $brand->name ?? '' }}" name="name">
							</div>
							<div class="form-group">
								<label>{{ trans('admin.website_url') }}</label>
								<input type="text" class="form-control" value="{{ $brand->website ?? '' }}" name="website">
							</div>
							<br>
							<button class="btn btn-success waves-effect width-md waves-light" type="submit">{{ trans('admin.save') }}</button>
						</div>
					</form>

					
				</div>
			</div>
		</div>
	</div>
</div>

@endsection 