@extends('admin.layouts.master')
@section('content')

<div class="wrapper">
	<div class="container-fluid">
		<div class="row" style="padding-top: 50px">
			<div class="col-md-12">
				<div class="card-box">
					<div style="display: flex; vertical-align: middle; justify-content: space-between; align-items: center;">
					
						<h4  class="header-title">{{ trans('admin.products') }}</h4>
						<a href="/{{ app()->getLocale() }}/admin/products/create" class="btn btn-success waves-effect width-md waves-light" >{{ trans('admin.add_product') }}</a>
					</div>
					<br>
					<br>
	
					<div class="table-responsive">
						<table class="table table-dark mb-0">
							<thead>
							<tr>
								<th>#</th>
								<th>{{ trans('admin.brand_name') }}</th>
								<th>{{ trans('admin.model_no') }}</th>
								<th>{{ trans('admin.brand') }}</th>
								<th>{{ trans('admin.edit') }}</th>
								<th>{{ trans('admin.delete') }}</th>
							</tr>
							</thead>
							<tbody>
							
							@foreach ($products as $key => $product)
								
							<tr>
								<th>{{ $key + 1 }}</th>
								<th>{{ $product->name }}</th>
								<th>{{ $product->model_number }}</th>
								<th>{{ $product->brand->name }}</th>
								<th><a href="/{{ app()->getLocale() }}/admin/products/{{ $product->id }}/edit" class="btn btn-success waves-effect width-md waves-light">{{ trans('admin.edit') }}</a></th>
								<th>
									<form method="post" action="/{{ app()->getLocale() }}/admin/products/{{ $product->id }}"> 
										@csrf
										@method('DELETE')
										<button type="submit" class="btn btn-success waves-effect width-md waves-light">{{ trans('admin.delete') }}</button>
									</form>
								</th>
							</tr>
							@endforeach
							</tbody>
						</table>
					</div>
					<br>
					{{ $products->links() }}
				</div>
			</div>
		</div>
	</div>
</div>

@endsection 