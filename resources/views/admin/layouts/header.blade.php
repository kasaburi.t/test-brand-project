<!-- Navigation Bar-->
<header id="topnav">

	<!-- Topbar Start -->
	<div class="navbar-custom">
		<div class="container-fluid">
			<ul class="list-unstyled topnav-menu float-right mb-0">

				<li class="dropdown notification-list">
					<!-- Mobile menu toggle-->
					<a class="navbar-toggle nav-link">
						<div class="lines">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</a>
					<!-- End mobile menu toggle-->
				</li>

				<li class="d-none d-sm-block" style="padding: 24px">
					<a style="color: white" href="/{{ app()->getLocale() == 'en' ? 'fr' : 'en' }}/admin">{{ app()->getLocale() == 'en' ? 'FR' : 'EN' }}</a>
				</li>
	
				

				<li class="dropdown notification-list">
					<a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
						<span class="pro-user-name ml-1">
							{{ \Auth::user()->name }} <i class="mdi mdi-chevron-down"></i> 
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right profile-dropdown ">
						<!-- item-->
						<div class="dropdown-header noti-title">
							<h6 class="text-overflow m-0">{{ Auth::user()->email }}</h6>
							<form method="post" action="/{{ app()->getLocale() }}/logout"> 
								@csrf
								<button type="submit" style="border: none; background-color: transparent; padding: 18px 0 0; color: white;" class="">{{ trans('admin.logout') }}</button>
							</form>
							<h6></h6>
						</div>

						

					</div>
				</li>


			</ul>

			

		</div> <!-- end container-fluid-->
	</div>
	<!-- end Topbar -->

	<div class="topbar-menu">
		<div class="container-fluid">
			<div id="navigation">
				<!-- Navigation Menu-->
				<ul class="navigation-menu">

					<li class="has-submenu">
						<a href="/{{ app()->getLocale() }}/admin"><i class="mdi mdi-view-dashboard"></i>{{ trans('admin.dashboard') }}</a>
					</li>

					<li><a href="/{{ app()->getLocale() }}/admin/brands">{{ trans('admin.brands') }}</a></li>
					<li><a href="/{{ app()->getLocale() }}/admin/products">{{ trans('admin.products') }}</a></li>

				</ul>
				<!-- End navigation menu -->

				<div class="clearfix"></div>
			</div>
			<!-- end #navigation -->
		</div>
		<!-- end container -->
	</div>
	<!-- end navbar-custom -->

</header>
<!-- End Navigation Bar-->
