<?php

return [
    'brands' => 'Marques',
	'brand_name' => 'Brand Name',
	'website_url' => 'URL du site',
	'add_brand' => 'Ajouter une marque',
	'name' => 'Nom',
	'website_url' => 'URL du site Web',
	'en' => 'EN',
	'fr' => 'FR',
	'save' => 'Enregistrer',
	'edit_brand' => "Modifier la marque",
	'edit' => "Modifier",
	'delete' => "Supprimer",
	'dashboard' => "Tableau de bord",
	'brands' => "Marques",
	'products' => "Produits",
	'add_product' => 'Ajouter un produit',
	'products' => 'Produits',
	'model_no' => 'Numéro de modèle',
	'logout' => 'Se déconnecter'
];