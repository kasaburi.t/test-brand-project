<?php

return [

    'brands' => 'Brands',
	'brand_name' => 'Brand Name',
	'website_url' => 'Website Url',
	'add_brand' => 'Add Brand',
	'name' => 'Name',
	'website_url' => 'Website URL',
	'en' => 'EN',
	'fr' => 'FR',
	'save' => 'Save',
	'edit_brand' => "Edit Brand",
	'edit' => "Edit",
	'delete' => "Delete",
	'dashboard' => "Dashboard",
	'brands' => "Brands",
	'products' => "Products",
	'add_product' => 'Add Product',
	'products' => 'Products',
	'model_no' => 'Model Number',
	'logout' => 'Log Out'
];


