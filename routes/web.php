<?php

use App\Http\Controllers\BrandController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// This code woll disable registration reset and verification. I know you only asked to disable Registration but we do not need verification also. -----------

\Auth::routes([
	'register' => false,
	'verify' => false, 
]);


Route::group([
    'middleware' => 'auth',
    'prefix' => 'admin'
], function () {
	
	Route::get('/', [DashboardController::class, 'index'])->name('dashboard');


	Route::resource('/brands', BrandController::class);
	Route::resource('/products', ProductController::class);

});
