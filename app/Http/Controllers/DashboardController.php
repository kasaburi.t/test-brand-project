<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Product;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){

		$brands = Brand::limit(5)->get();
		$products = Product::limit(5)->get();
		return view('admin.dashboard', compact('brands', 'products'));
	}
}
