<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;


	protected $fillable = [
        'name',
        'model_number', 
        'brand_id'
    ];

	/**
	 * Get the Brand associated with the Product
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function brand()
	{
		return $this->hasOne(Brand::class, 'id', 'brand_id');
	}

}
